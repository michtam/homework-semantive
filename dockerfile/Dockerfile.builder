FROM openjdk:8
MAINTAINER marcin.michta@gmail.com

ENV TZ="Europe/Warsaw"
ENV DEBIAN_FRONTEND=noninteractive
ENV GIT_REPO="https://github.com/Semantive/cassandra-trainings-crud-template"

WORKDIR /opt/java_app
VOLUME /opt/target/ 

RUN 	apt update && \
	apt install --yes --no-install-recommends software-properties-common git curl gnupg tzdata apt-transport-https ca-certificates && \
	echo "deb https://dl.bintray.com/sbt/debian /" > /etc/apt/sources.list.d/sbt.list && \
	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
	apt update && \
	apt install -y sbt && \
	rm -rf /var/lib/apt/lists/*
RUN 	git clone ${GIT_REPO} && \
	cd cassandra-trainings-crud-template/cassandra-crud-rest && \
	sbt dist


